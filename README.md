# pygpt4all-service


A quick and dirty FastAPI service on top of pygpt4all.

* https://github.com/nomic-ai/pygpt4all
* https://fastapi.tiangolo.com/
* https://www.uvicorn.org/


# Quick Start

Start the service,

``` bash
uvicorn main:app --reload --host 0.0.0.0
```

The swagger docs will be served at two locations (just different themes).

* `http://<ipaddr>:8000/docs`
* `http://<ipaddr>:8000/redoc`

You can generate a request via those docs.
Alternatively you can use curl,

``` bash
curl -X 'POST' \
  'http://<ipaddr>:8000/generate' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "input_text": "once upon a time",
  "seed": 827,
  "n_predict": 20,
  "n_threads": 4,
  "repeat_last_n": 64,
  "top_k": 40,
  "top_p": 0.95,
  "temp": 0.8,
  "repeat_penalty": 1.1,
  "n_batch": 8
}'
```

You can also use the Python requests package,

``` python
import requests
response = requests.post(
    "http://<ipaddr>:8000/generate",
    json={"input_text": "once upon a time"},
)
```
