from pygpt4all.models.gpt4all import GPT4All

def new_text_callback(text):
    print(text, end="", flush=True)

model_path = "../models/gpt4all-lora-quantized-pylammacpp-converted.bin"

model = GPT4All(model_path)
output = model.generate(
    "Once upon a time",
    n_predict=55,
    new_text_callback=new_text_callback,
    verbose=True
)
