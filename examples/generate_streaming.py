import requests


with requests.post(
    "http://localhost:8000/generate_stream",
    json={
        "input_text": "once upon a time",
        "n_predict": 50,
    },
    stream=True,
) as r:
    chunks = []
    for chunk in r.iter_content(256):  # or, for line in r.iter_lines():
        if chunk:
            print(chunk.decode())
            chunks.append(chunk.decode())

print("".join(chunks))
