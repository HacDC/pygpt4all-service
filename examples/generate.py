import requests

response = requests.post(
    "http://localhost:8000/generate",
    json={
        "input_text": "once upon a time",
        "n_predict": 20,
    },
)
