from functools import partial
import logging
import os
from threading import Thread
from typing import Callable
import time
import uuid

from fastapi import FastAPI
from fastapi.responses import StreamingResponse
from pydantic import BaseModel
from pygpt4all.models.gpt4all import GPT4All


logger = logging.getLogger(__name__)


model_path = "../models/gpt4all-lora-quantized-pylammacpp-converted.bin"
n_ctx = 1024
model = GPT4All(model_path, n_ctx=n_ctx)


app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


class GenerateRequest(BaseModel):
    """Request to generate text.

    https://nomic-ai.github.io/pygpt4all/
    """
    input_text: str
    seed: int | None = 827
    n_predict: int | None = 10
    n_threads: int | None = 4
    repeat_last_n: int | None = 64
    top_k: int | None = 40
    top_p: float | None = 0.95
    temp: float | None = 0.8
    repeat_penalty: float | None = 1.1
    n_batch: int | None = 8


class GenerateResponse(BaseModel):
    generated_text: str


def model_generate(
    generate_request: GenerateRequest,
    new_text_callback: Callable[str, str] = lambda x: None,
) -> str:
    logger.warning("MODEL GENERATE")
    return model.generate(
        generate_request.input_text,
        seed=generate_request.seed,
        n_predict=generate_request.n_predict,
        n_threads=generate_request.n_threads,
        repeat_last_n=generate_request.repeat_last_n,
        top_k=generate_request.top_k,
        top_p=generate_request.top_p,
        temp=generate_request.temp,
        repeat_penalty=generate_request.repeat_penalty,
        new_text_callback=new_text_callback,
    )


@app.post("/generate")
def generate_text(generate_request: GenerateRequest) -> GenerateResponse:
    generated_text = model_generate(generate_request)
    return GenerateResponse(generated_text=generated_text)


def new_text_callback(fpath, text):
    with open(fpath, "a") as fp:
        fp.write(f"{text}\n")
    print(text, end="", flush=True)


def file_text_streamer(fpath):
    logger.warning("ENTER FILE STREAM")

    # wait for file to exist
    start_time = time.time()
    while True:
        now_time = time.time()

        if os.path.exists(fpath):
            logger.warning("FILE STREAM PATH EXISTS")
            break

        dt = now_time - start_time
        if dt > 5:
            logger.warning("FILE STREAM PATH NEVER EXISTED")
            return

        time.sleep(0.1)

    start_time = time.time()
    yielded_chunks = []
    while True:
        now_time = time.time()

        with open(fpath, "r") as fp:
            chunks = [el.rstrip("\n") for el in fp.readlines()]

        if len(chunks) > len(yielded_chunks):
            for chunk in chunks[len(yielded_chunks):]:
                yield chunk
            yielded_chunks = chunks
            last_yield_time = now_time

        dt = now_time - last_yield_time
        if dt > 2.0:
            logger.warning("NO YIELD for 2 seconds, breaking")
            break

        dt = now_time - start_time
        if dt > 120:
            logger.warning("max time reached, breaking")
            break

        time.sleep(0.1)

    time.sleep(0.5)
    os.system(f"rm {fpath}")


@app.post("/generate_stream")
def generate_text_stream(generate_request: GenerateRequest) -> GenerateResponse:

    logger.warning("starting text stream")
    fpath = "{}.chunks".format(str(uuid.uuid4()))

    t = Thread(
        target=model_generate,
        args=(generate_request, partial(new_text_callback, fpath)),
    )
    t.start()

    return StreamingResponse(
        file_text_streamer(fpath),
        media_type="text/event-stream",
    )
